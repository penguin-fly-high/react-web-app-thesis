import React, { Component } from 'react'

export default class BuyerSearch extends Component {
  render() {
    return (
      <div>
        <div className="container">
        <h1>Buyer Search Page</h1>
        <div className="card mb-3">
          <div className="card-body">
            <form>
              <div className="form-group">
                <label htmlFor="product_name" style={{ float: "left" }}>
                  Product Name
                </label>
                <input
                  id="product_name"
                  type="text"
                  className="form-control"
                  placeholder="Product Name"
                />
              </div>

              <div className="form-group " style={{ marginLeft: "0px" }}>
                {/*Menu item*/}
                <select class="form-control ">
                  <option value="volvo">Volvo</option>
                  <option value="saab">Saab</option>
                  <option value="opel">Opel</option>
                  <option value="audi">Audi</option>
                </select>
              </div>

              <div className="form-group">
              <label htmlFor="city" style={{ float: "left" }}>
                  City
                </label>
                <input
                  id="city"
                  type="text"
                  className="form-control"
                  placeholder="City"
                />
              </div>

              <div className="form-group">
                <label htmlFor="location" style={{ float: "left" }}>
                  Location
                </label>
                <input
                  id="location"
                  type="text"
                  className="form-control"
                  placeholder="Location"
                />
              </div>

              <div className="form-group">
                <label htmlFor="location" style={{ float: "left" }}>
                  Location
                </label>
                <input
                  id="location"
                  type="text"
                  className="form-control"
                  placeholder="Location"
                />
              </div>

              <div className="form-group">
                <label htmlFor="grown-up-period" style={{ float: "left" }}>
                  Grown up period
                </label>
                <input
                  id="grown-up-period"
                  type="text"
                  className="form-control"
                  placeholder="grown up period"
                />
              </div>
              <button className="btn btn-primary btn-large">Search</button>
            </form>
          </div>
        </div>
      </div>
      </div>
    )
  }
}
