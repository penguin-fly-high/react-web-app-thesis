import React, { Component } from "react";

export default class FarmerPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showMenuItem: false
    };
  }

  componentDidMount() {
    console.log(this.state.showMenuItem);
    this.setState({
      showMenuItem: false
    });
  }

  showMenu = e => {
    console.log(this.state.showMenuItem);
    e.preventDefault();
    this.setState({
      showMenuItem: !this.state.showMenuItem
    });
  };

  render() {
    return (
      <div className="container">
        <h1>Farmer Posting Page</h1>
        <div className="card mb-3">
          <div className="card-body">
            <form>
              <div className="form-group">
                <label htmlFor="product_name" style={{ float: "left" }}>
                  Product Name
                </label>
                <input
                  id="product_name"
                  type="text"
                  className="form-control"
                  placeholder="Product Name"
                />
              </div>

              <div className="form-group row" style={{ marginLeft: "2px" }}>
                {/*Menu item*/}
                <select class="form-control form-control-lg">
                  <option value="volvo">Volvo</option>
                  <option value="saab">Saab</option>
                  <option value="opel">Opel</option>
                  <option value="audi">Audi</option>
                </select>
              </div>

              <div className="form-group" >
                {/*Menu item*/}
                <select class="form-control form-control-lg">
                  <option value="volvo">Volvo</option>
                  <option value="saab">Saab</option>
                  <option value="opel">Opel</option>
                  <option value="audi">Audi</option>
                </select>
              </div>

              <div className="form-group">
                <label htmlFor="quantity" style={{ float: "left" }}>
                  Quantity
                </label>
                <input
                  id="quantity"
                  type="text"
                  className="form-control"
                  placeholder="Quantity"
                />
              </div>

              <div className="form-group">
                <label htmlFor="location" style={{ float: "left" }}>
                  Location
                </label>
                <input
                  id="location"
                  type="text"
                  className="form-control"
                  placeholder="Location"
                />
              </div>

              <div className="form-group">
                <label htmlFor="grown-up-period" style={{ float: "left" }}>
                  Grown up period
                </label>
                <input
                  id="grown-up-period"
                  type="text"
                  className="form-control"
                  placeholder="grown up period"
                />
              </div>
              <button className="btn btn-primary btn-large">Post</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
