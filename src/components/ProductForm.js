import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Form, FormGroup, Label, Input, Button } from 'reactstrap';

export default class ProductForm extends PureComponent {
    constructor(props) {
        super(props);

        // Initialize states
        this.state = { 
            form: {
                name: '',
                classification: '',
                category: '',
                farmer: -1,
                ...props.form
            }, 
            data: {
                farmers: [ ...props.data.farmers ]
            }
        };
    }

    handleInputChange({ target }) {
        this.setState({
            ...this.state,
            form: {
                ...this.state.form,
                [target.name]: target.value
            }
        });
    }
    
    render() {
        const { data, form } = this.state;
        const { onSubmit: submitHandler } = this.props;
        
        return (
            <Form>
                <FormGroup>
                    <Label for="name">Name</Label>
                    <Input type="text" name="name" id="name" 
                        defaultValue={ form.name } onChange={ (e) => this.handleInputChange(e) }/>
                </FormGroup>
                <FormGroup>
                    <Label for="location">Location</Label>
                    <Input type="text" name="location" id="location" onChange={ (e) => this.handleInputChange(e) }/>
                </FormGroup>
                <FormGroup>
                    <Label for="classification">Classification</Label>
                    <Input type="text" name="classification" id="classification" 
                        defaultValue={ form.classification } onChange={ (e) => this.handleInputChange(e) }/>
                </FormGroup>
                <FormGroup>
                    <Label for="category">Category</Label>
                    <Input type="text" name="category" id="category" 
                        defaultValue={ form.category } onChange={ (e) => this.handleInputChange(e) }/>
                </FormGroup>
                <FormGroup>
                    <Label for="farmer">Farmer</Label>
                    <Input type="select" name="farmer" id="farmer" 
                        defaultValue={ form.farmer } onChange={ (e) => this.handleInputChange(e) }>
                        { data.farmers.map(({ id, name }) => <option value={ id }>{ name }</option>) }
                    </Input>
                </FormGroup>
                <Button color="primary" onClick={ () => submitHandler(this.state.form) }>Submit</Button>
            </Form>
        );
    }
}

ProductForm.propTypes = {
    onSubmit: PropTypes.func
};

ProductForm.defaultProps = {
    onSubmit: () => {},
    form: {},
    data: {
        farmers: []
    }
};