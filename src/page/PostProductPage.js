import React, { PureComponent } from 'react';
import { Container, Card, CardHeader, CardBody, Row, Col } from 'reactstrap';
import Axios from 'axios';
import PropTypes from 'prop-types';
import ProductForm from '../components/ProductForm';

export default class PostProductPage extends PureComponent {

    onComponentDidMount() {
        // TODO: get list of farmers
    }

    postData(data) {
        const mutationPayload = `
            mutation {
                addProduct(
                    name: "${ data.name }", 
                    classification: "${ data.classification }", 
                    category: "${ data.category }", 
                    location: "${ data.location }", 
                    farmerId: "${ data.farmer }"
                    ) {
                        name
                    }
            }
        `;
        Axios({
            url: 'http://localhost:4000/graphql',
            method: 'post',
            headers: {'Content-Type': 'application/json'},
            data: {
                query: mutationPayload
            }
        }).then(res => console.log(res)).catch(err => console.log(err))
    }

    editData(data) {
        // TODO Implementation
        const mutationPayload = `
            mutation {
                editProduct(
                    id: "${ this.props.id }",
                    name: "${ data.name }", 
                    classification: "${ data.classification }", 
                    category: "${ data.category }", 
                    location: "${ data.location }", 
                    farmerId: "${ data.farmer }"
                    )
            }
        `;
        Axios.post('http://localhost:4000/graphql', mutationPayload);
    }

    handleSubmit(data) {
        if (this.props.id) {
            this.editData(data);
        } else {
            this.postData(data);
        }
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col md={5}>
                        <Card>
                            <CardHeader>Product Form</CardHeader>
                            <CardBody>
                                <ProductForm onSubmit={ data => this.handleSubmit(data) }/>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </Container>
        )
    }
}

PostProductPage.propTypes = {
    id: PropTypes.number // Product id number
}

PostProductPage.defaultProps = {
    id: null
};