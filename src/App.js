import React, { Component } from 'react';
import './App.css';
import PostProductPage from './page/PostProductPage';

class App extends Component {
  render() {
    return (
      <div className="App">
        <PostProductPage/>
      </div>
    );
  }
}

export default App;